#!/bin/sh

sudo date -s "$(wget -qSO- --max-redirect=0 192.168.0.1 2>&1 | grep Date: | cut -d' ' -f5-8)Z"